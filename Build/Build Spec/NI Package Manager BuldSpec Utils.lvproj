<?xml version="1.0" encoding="UTF-8" standalone="no" ?><Project LVVersion="17008000" Type="Project">
	<Property Name="G3Tag:-:G3AutoDocPrinterReportComboTag:-:G3_Documentation Reports.lvlib:Project Report.lvclass::G3_Documentation Reports.lvlib:Confluence-JIRA.lvclass" Type="Str">196437368</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"/>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:BitBucket Wiki Markdown.lvclass" Type="Str">C:\PetranWay\Wikis\NI Package Manager Bld Spec Utils\Developer Resources\APIs\NI Package Manager BuldSpec Utils Project Documentation.md</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:Confluence.lvclass" Type="Str">237371470</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Build \ Install Stuff" Type="Folder">
			<Item Name="NIPKG" Type="Folder">
				<Item Name="NIPKG_LV_NIPKG_BldSpec_Utils_BuildSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/NIPKG_LV_NIPKG_BldSpec_Utils_BuildSteps.lvlib"/>
				<Item Name="NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib" Type="Library" URL="../../Custom Build Steps/NIPKG_LV/NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib"/>
				<Item Name="VI Launcher.bat" Type="Document" URL="/&lt;userlib>/_PetranWay/Custom Install Step Launcher/Source/VI Launcher.bat"/>
			</Item>
			<Item Name="VIP" Type="Folder">
				<Item Name="Post-Build Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Build Custom Action.vi"/>
				<Item Name="Post-Install Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Install Custom Action.vi"/>
				<Item Name="Post-Uninstall Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Post-Uninstall Custom Action.vi"/>
				<Item Name="Pre-Build Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Build Custom Action.vi"/>
				<Item Name="Pre-Install Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Install Custom Action.vi"/>
				<Item Name="Pre-Uninstall Custom Action.vi" Type="VI" URL="../../Custom Build Steps/VIP/Pre-Uninstall Custom Action.vi"/>
			</Item>
			<Item Name="Build Spec" Type="Folder">
				<Item Name="NIPKG_LV" Type="Folder">
					<Item Name="SubSpecs" Type="Folder">
						<Item Name="SrcDist.ini" Type="Document" URL="../NIPKG_LV/SubSpecs/SrcDist.ini"/>
					</Item>
					<Item Name="Commit Behavior.ini" Type="Document" URL="../NIPKG_LV/Commit Behavior.ini"/>
					<Item Name="NIPKG_LV.ini" Type="Document" URL="../NIPKG_LV/NIPKG_LV.ini"/>
				</Item>
				<Item Name="VIP" Type="Folder">
					<Item Name="Commit Behavior.ini" Type="Document" URL="../VIP/Commit Behavior.ini"/>
					<Item Name="VIPackage.ini" Type="Document" URL="../VIP/VIPackage.ini"/>
				</Item>
				<Item Name="VIPackage.vipb" Type="Document" URL="../VIPackage.vipb"/>
				<Item Name="Software Module Definition.ini" Type="Document" URL="../Software Module Definition.ini"/>
			</Item>
			<Item Name="License.rtf" Type="Document" URL="../../License/License.rtf"/>
		</Item>
		<Item Name="Source" Type="Folder" URL="../../../Source">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib>/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib>/Utility/error.llb/whitespace.ctl"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib>/xml/NI_XML.lvlib"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib>/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="JSONtext.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JSONtext/JSONtext.lvlib"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib>/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="JDP Utility.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JDP Science Common Utilities/JDP Utility.lvlib"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib>/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="JDP Timestamp.lvlib" Type="Library" URL="/&lt;vilib>/JDP Science/JDP Science Common Utilities/Timestamp/JDP Timestamp.lvlib"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib>/Numeric/LVNumericRepresentation.ctl"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="PetranWay_SrcDist BldSpec Utils.lvlib" Type="Library" URL="/&lt;userlib>/_PetranWay/SrcDist BldSpec Utils/Source/PetranWay_SrcDist BldSpec Utils.lvlib"/>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SrcDist" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{99DCA500-F991-405D-89C5-28D0F3B70CA8}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SrcDist</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/17.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">/C/Users/labview/Documents/LabVIEW Data/2017(32-bit)/ExtraVILib</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[5]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[5].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">6</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_BuildSteps.lvlib/Post-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_preActionVIID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_BuildSteps.lvlib/Pre-Build Action_SrcDist.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{3CA134DB-F5EC-408A-8627-84EB5E9F7C49}</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist/NIPKG_BldSpec_Utils_SrcDist_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">CustomInstallSteps</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist/CustomInstallSteps</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">NIPKGCustomInstallSteps</Property>
				<Property Name="Destination[3].path" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist/CustomInstallSteps/NIPKG</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].destName" Type="Str">License</Property>
				<Property Name="Destination[4].path" Type="Path">/C/builds/NIPKG_BldSpec_Utils/SrcDist/License</Property>
				<Property Name="Destination[4].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">5</Property>
				<Property Name="Source[0].itemID" Type="Str">{90D0AA77-25FF-4A77-8603-10C1424C01D2}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Source</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib/Post-Install All_NIPKG.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib/Post-Install_NIPKG.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/NIPKG_LV_NIPKG_BldSpec_Utils_InstallSteps.lvlib/Pre-Uninstall_NIPKG.vi</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[7].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Build \ Install Stuff/License.rtf</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">9</Property>
			</Item>
			<Item Name="NIPKG" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_license" Type="Ref">/My Computer/Build \ Install Stuff/License.rtf</Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">


&lt;Upgrade Information&gt;

   Upgraded:    petranway-custominstallsteplauncher-nipkg-lv
           From:    1.1.0-28
              To:     1.1.0-29

&lt;/Upgrade Information&gt;</Property>
				<Property Name="PKG_actions.Count" Type="Int">3</Property>
				<Property Name="PKG_actions[0].Arguments" Type="Str">2017 32 "Run VI=Post-Install_NIPKG.vi"</Property>
				<Property Name="PKG_actions[0].NIPKG.HideConsole" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.Schedule" Type="Str">post</Property>
				<Property Name="PKG_actions[0].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[0].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[1].Arguments" Type="Str">2017 32 "Run VI=Post-Install All_NIPKG.vi" "Mass Compile=True"</Property>
				<Property Name="PKG_actions[1].NIPKG.HideConsole" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[1].NIPKG.Schedule" Type="Str">postall</Property>
				<Property Name="PKG_actions[1].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[1].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_actions[2].Arguments" Type="Str">2017 32 "Run VI=Pre-Uninstall_NIPKG.vi"</Property>
				<Property Name="PKG_actions[2].NIPKG.HideConsole" Type="Bool">true</Property>
				<Property Name="PKG_actions[2].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[2].NIPKG.Schedule" Type="Str">pre</Property>
				<Property Name="PKG_actions[2].NIPKG.Step" Type="Str">uninstall</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Child" Type="Ref">/My Computer/Build \ Install Stuff/NIPKG/VI Launcher.bat</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_actions[2].NIPKG.Target.Source" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_actions[2].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[2].Type" Type="Str">NIPKG.Batch</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">false</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">false</Property>
				<Property Name="PKG_buildNumber" Type="Int">16</Property>
				<Property Name="PKG_buildSpecName" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">1</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"/>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">1.1.0-29</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">Custom Install Step Launcher - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">petranway-custominstallsteplauncher-nipkg-lv</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">Launches the VI responsible for custom install \ unsinstall behavior.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies[1].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MaxVersion" Type="Str"/>
				<Property Name="PKG_dependencies[1].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MinVersion" Type="Str">1.0.0-4</Property>
				<Property Name="PKG_dependencies[1].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[1].NIPKG.DisplayName" Type="Str">Custom Install Step Launcher - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[1].Package.Name" Type="Str">toolchainsupport-custominstallsteplauncher-nipkg-lv</Property>
				<Property Name="PKG_dependencies[1].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[1].Package.Synopsis" Type="Str">Launches the VI responsible for custom install \ unsinstall behavior.</Property>
				<Property Name="PKG_dependencies[1].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[1].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str"/>
				<Property Name="PKG_destinations.Count" Type="Int">6</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{016CC1B2-264C-470C-B093-E7CA1270212F}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">user.lib</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">{2BB90BCF-9A1D-4DB7-B83D-56F563D1ACB2}</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{2BB90BCF-9A1D-4DB7-B83D-56F563D1ACB2}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">LabVIEW 2017</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[2].ID" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_destinations[2].Subdir.Directory" Type="Str">National Instruments</Property>
				<Property Name="PKG_destinations[2].Subdir.Parent" Type="Str">root_5</Property>
				<Property Name="PKG_destinations[2].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[3].ID" Type="Str">{97E511ED-1CDA-4207-B09C-834AB3D93DD9}</Property>
				<Property Name="PKG_destinations[3].Subdir.Directory" Type="Str">NI Package Manager BldSpec Utils</Property>
				<Property Name="PKG_destinations[3].Subdir.Parent" Type="Str">{BED7F222-5FE7-4320-A89B-9B4BE295E307}</Property>
				<Property Name="PKG_destinations[3].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[4].ID" Type="Str">{BED7F222-5FE7-4320-A89B-9B4BE295E307}</Property>
				<Property Name="PKG_destinations[4].Subdir.Directory" Type="Str">_PetranWay</Property>
				<Property Name="PKG_destinations[4].Subdir.Parent" Type="Str">{016CC1B2-264C-470C-B093-E7CA1270212F}</Property>
				<Property Name="PKG_destinations[4].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[5].ID" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_destinations[5].Subdir.Directory" Type="Str">Source</Property>
				<Property Name="PKG_destinations[5].Subdir.Parent" Type="Str">{97E511ED-1CDA-4207-B09C-834AB3D93DD9}</Property>
				<Property Name="PKG_destinations[5].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">NI Package Manager BldSpec Utils - NIPKG - LV</Property>
				<Property Name="PKG_displayVersion" Type="Str"/>
				<Property Name="PKG_homepage" Type="Str">https://bitbucket.org/ChrisCilino/ni-package-manager-bldspec-utils</Property>
				<Property Name="PKG_lvrteTracking" Type="Bool">false</Property>
				<Property Name="PKG_maintainer" Type="Str">PetranWay &lt;christopher.cilino@petranway.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/builds/NIPKG_BldSpec_Utils/NIPKG</Property>
				<Property Name="PKG_packageName" Type="Str">petranway-nipkg-bldspec-utils-nipkg-lv</Property>
				<Property Name="PKG_ProviderVersion" Type="Int">1810</Property>
				<Property Name="PKG_section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">0</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{CF8E20D1-06E1-4590-AA59-E084D90E3401}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">Build</Property>
				<Property Name="PKG_sources[1].Destination" Type="Str">{6EE9AACE-AA65-470E-A576-5DCCB3FAF13A}</Property>
				<Property Name="PKG_sources[1].ID" Type="Ref"/>
				<Property Name="PKG_sources[1].Type" Type="Str">File</Property>
				<Property Name="PKG_synopsis" Type="Str">A series of utilities to interact with ni package build specifications of a labview project.</Property>
				<Property Name="PKG_version" Type="Str">1.1.0</Property>
			</Item>
		</Item>
	</Item>
</Project>